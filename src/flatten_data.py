import glob, os, math, numpy as np

def getlastline(filepath):
	file = open(filepath, "r")
	linelist = file.readlines()
	file.close()
	
	line = linelist[-1]
	return line[:len(line) - 1] # Remove new line character

# Maths
def mean(numbers):
	return float(sum(numbers)) / max(len(numbers), 1)

def variance(numbers, mean):
	sum = 0
	for number in numbers:
		sum = sum + (number - mean) ** 2
		
	return float(sum / max(len(numbers), 1))

def stdev(numbers):
	return math.sqrt(variance(numbers, mean(numbers)))

# ------- Main ---------
os.chdir("..")

outpath = 'out.txt'

if os.path.isfile(outpath):
	os.remove(outpath)

fout = open(outpath, 'w+')

folder = '3D'

<<<<<<< HEAD
for problem in os.listdir(folder):
	fout.write('----%s-----\n' % problem)

	for algorithm in os.listdir(folder + "/" + problem):
		fout.write('-%s-\n' % algorithm)

		path = folder + "/" + problem + "/" + algorithm + "/"
	   
		hvlist = []
	#	igdlist = []

		for i in os.listdir(path):
			index = int(i) - 1
			hvlist.insert(index, float(getlastline(path + i + '/HV_' + i + '.txt')))
	#		igdlist.insert(index, float(getlastline(path + i + '/IGD_' + i + '.txt')))
		
		maxHV = max(hvlist)
		maxid = hvlist.index(maxHV)
	#	maxIGD = igdlist[maxid]
		
		minHV = min(hvlist)
		minid = hvlist.index(minHV)
	#	minIGD = igdlist[minid]
		
		pathMax = './' + problem + '/' + algorithm + '/' + str(maxid + 1) + '/img'
		pathMin = './' + problem + '/' + algorithm + '/' + str(minid + 1) + '/img'
				
		fout.write('\\begin{figure}[H] \n' \
						'\t\\centering\n' \
						'\t\\begin{subfigure}{.5\\textwidth}\n' \
							'\t\t\\centering\n' \
							'\t\t\\includegraphics[width=\\linewidth]{{%s}.png}\n' \
							'\t\t\\caption{Best case. Hypervolume: %s.}\n' \
						#	'\t\t\\caption{Best case. Hypervolume: %s. IGD: %s.}\n' \
						'\t\\end{subfigure}%%\n' \
						'\t\\begin{subfigure}{.5\\textwidth}\n' \
							'\t\t\\centering\n' \
							'\t\t\\includegraphics[width=\\linewidth]{{%s}.png}\n' \
							'\t\t\\caption{Worst case. Hypervolume: %s.}\n' \
						#	'\t\t\\caption{Worst case. Hypervolume: %s. IGD: %s.}\n' \
						'\t\\end{subfigure}\n' \
						'\t\\caption{Results of %s on %s} \n' \
					'\\end{figure}\n' %
			(pathMax, round(maxHV, 3), #round(maxIGD, 3),
			 pathMin, round(minHV, 3), #round(minIGD, 3),
			 algorithm, problem.split('_', 1)[0]))	
	
	fout.write('\n')
	
	#	fout.write("%.3f\n" % round(mean_, 3))
	#	fout.write("%.3f\n" % round(stdev_, 3))
	#	fout.write("%.3f %s\n" % (round(max_, 3), maxid))
	#	fout.write("%.3f\n" % round(toppercent, 3))		  
	#	fout.write("%.3f\n" % round(middlepercent, 3))
	#	fout.write("%.3f\n" % round(botpercent, 3))
	#	fout.write("%.3f %s\n" % (round(min_, 3), minid))
=======
for algorithm in os.listdir(folder):
    fout.write('----%s-----\n' % algorithm)

    for problem in os.listdir(folder + "/" + algorithm):
        fout.write('-%s-\n' % problem)

        path = folder + "/" + algorithm + "/" + problem + "/"
       
        hvlist = []
        igdlist = []

        for hv in glob.glob(path + '*/HV_*.txt'):
            hvlist.append(float(getlastline(hv)))
            
        for igd in glob.glob(path + '*/IGD_*.txt'):
            igdlist.append(float(getlastline(igd)))
        
        fout.write("%.3f\n" % round(mean(hvlist), 3))
        fout.write("%.3f\n" % round(stdev(hvlist), 3))
        fout.write("%.3f\n" % round(max(hvlist), 3))
        fout.write("%.3f\n" % round(np.percentile(hvlist, 75), 3))        
        fout.write("%.3f\n" % round(np.percentile(hvlist, 50), 3))
        fout.write("%.3f\n" % round(np.percentile(hvlist, 25), 3))
        fout.write("%.3f\n" % round(min(hvlist), 3))
        
    fout.write('\n')
>>>>>>> b3d06ecc4b8c187bd5aeefe53a0dd91dceaed635

fout.close()
